What is ``buildbox-worker``?
============================

``buildbox-worker`` is a program that accepts jobs from a build server, invokes
a ``buildbox-run`` command to run them, and sends the results back. It
implements Bazel's `Remote Workers API`_.

It is designed to work with `BuildGrid`_ but does not depend on it.

.. _Remote Workers API: https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU
.. _BuildGrid: https://buildgrid.build


Usage
=====

  buildbox-worker [OPTIONS] [BOT_ID]

This starts a worker with the given bot ID. (If no bot ID is specified, a
default one is generated using the machine's hostname and ``buildbox-worker``'s
process ID.)

Options:

* --concurrent-jobs=JOBS

  Number of jobs to run at once. If this isn't specified, jobs will be run one
  at a time.

* --stop-after=JOBS

  Terminate after running the given number of jobs. If this isn't specified,
  it's unlimited.

* --instance=INSTANCE

  Instance name to pass to the Remote Workers API server. Without this option,
  ``buildbox-worker`` will pass an empty string as the instance name.

* --platform KEY VALUE

  Add a key-value pair to the "Platform" message the worker sends to the
  server. See the Remote Workers API specification for keys and values you can
  use.

* --buildbox-run=BUILDBOXRUN

  Change the command the worker uses to run jobs from ``buildbox-run`` to
  something else. The given command must support the same interface as
  ``buildbox-run`` (see below)

* --runner-arg=ARG

  Pass ``ARG`` to ``buildbox-run`` when the worker runs a job. This can be
  useful if the ``buildbox-run`` implementation you're using supports
  non-standard options.

* --bots-remote=URL (required)

  The URL for the RWAPI server.

* --bots-server-cert=PATH

  Path to the PEM-encoded public server certificate for https connections to
  the RWAPI server. This allows use of self-signed certificates.

* --bots-client-key=PATH

  Path to the PEM-encoded private client key for https with
  certificate-based client authentication. If this is specified,
  ``--bots-client-cert`` must be specified as well.

* --bots-client-cert=PATH

  Path to the PEM-encoded public client certificate for https with
  certificate-based client authentication. If this is specified,
  ``--bots-client-key`` must be specified as well.

* --cas-remote=URL (required)

  The URL for the Content-Addressable Storage server that job-related files
  will be fetched from and uploaded to.

* --cas-server-cert=PATH

  Path to the PEM-encoded public server certificate for https connections to
  the CAS server. This allows use of self-signed certificates.

* --cas-client-key=PATH

  Path to the PEM-encoded private client key for https with
  certificate-based client authentication. If this is specified,
  ``--cas-client-cert`` must be specified as well.

* --cas-client-cert=PATH

  Path to the PEM-encoded public client certificate for https with
  certificate-based client authentication. If this is specified,
  ``--cas-client-key`` must be specified as well.

* --verbose

  Output additional information to stdout that may be useful when debugging.

The ``buildbox-run`` interface
==============================

``buildbox-worker`` invokes a ``buildbox-run`` command to download a job's
files, run the job, and upload the results. You can change the command with
the ``--buildbox-run=BUILDBOXRUN`` option to change the fetching and
sandboxing mechanisms the worker uses.

The command you specify must support the following interface:

  BUILDBOXRUN --remote=URL [--server-cert=CERT] [--server-key=KEY]
  [--client-key=KEY] --action=FILE --action-result=FILE

``--remote`` specifies the URL of the CAS server. ``--server-cert``,
``--server-key``, and ``--client-key`` work as they do in ``buildbox-worker``.
``--action`` specifies the path to a file with a serialized Action protocol
buffer, and ``--action-result`` is the path to write the corresponding
ActionResult to.
