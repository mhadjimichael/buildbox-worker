/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_worker.h>

#include <buildboxcommon_connectionoptions.h>

#include <cstring>
#include <iostream>
#include <string>
#include <unistd.h>

using namespace buildboxcommon;
using namespace buildboxworker;

static const int BUILDBOX_WORKER_USAGE_PAD_WIDTH = 32;

std::string generateDefaultBotID()
{
    char hostname[256] = {0};
    gethostname(hostname, sizeof(hostname));
    return std::string(hostname) + "-" + std::to_string(getpid());
}

static void usage(const char *name)
{
    std::cerr << "usage: " << name << " [OPTIONS] [BOT_ID]\n";
    std::cerr << "    --concurrent-jobs=JOBS      Number of jobs to run "
                 "concurrently\n";
    std::cerr << "    --stop-after=JOBS           Stop after running this "
                 "many jobs\n";
    std::cerr << "    --instance=INSTANCE         Pass an instance name to "
                 "the server\n";
    std::cerr << "    --platform KEY VALUE        Set a platform property\n";
    std::cerr
        << "    --buildbox-run=BUILDBOXRUN  Path to buildbox-run binary\n";
    std::cerr << "    --runner-arg=ARG            Pass ARG to buildbox-run\n";
    ConnectionOptions::printArgHelp(BUILDBOX_WORKER_USAGE_PAD_WIDTH, "Bots",
                                    "bots-");
    ConnectionOptions::printArgHelp(BUILDBOX_WORKER_USAGE_PAD_WIDTH, "CAS",
                                    "cas-");
    std::cerr << "    --verbose                   Enable verbose logging\n";
}

int main(int argc, char *argv[])
{
    const char *prgname = argv[0];
    argv++;
    argc--;

    Worker worker;

    while (argc > 0) {
        const char *arg = argv[0];
        const char *assign = strchr(arg, '=');
        if (worker.d_botsServer.parseArg(arg, "bots-") ||
            worker.d_casServer.parseArg(arg, "cas-")) {
            // Argument was handled by server_opts.
        }
        else if (arg[0] == '-' && arg[1] == '-') {
            arg += 2;
            if (assign) {
                const int key_len = assign - arg;
                const char *value = assign + 1;
                if (strncmp(arg, "concurrent-jobs", key_len) == 0) {
                    try {
                        worker.d_maxConcurrentJobs = std::stoi(value);
                    }
                    catch (const std::invalid_argument &) {
                        std::cerr << "Value must be a number: " << argv[0]
                                  << "\n";
                        usage(prgname);
                        return 1;
                    }
                }
                else if (strncmp(arg, "stop-after", key_len) == 0) {
                    try {
                        worker.d_stopAfterJobs = std::stoi(value);
                    }
                    catch (const std::invalid_argument &) {
                        std::cerr << "Value must be a number: " << argv[0]
                                  << "\n";
                        usage(prgname);
                        return 1;
                    }
                }
                else if (strncmp(arg, "instance", key_len) == 0) {
                    worker.d_instance = value;
                }
                else if (strncmp(arg, "buildbox-run", key_len) == 0) {
                    worker.d_buildboxRun = value;
                }
                else if (strncmp(arg, "runner-arg", key_len) == 0) {
                    worker.d_extraRunArgs.push_back(value);
                }
                else {
                    std::cerr << "Invalid option " << argv[0] << "\n";
                    usage(prgname);
                    return 1;
                }
            }
            else {
                if (strcmp(arg, "help") == 0) {
                    usage(prgname);
                    return 0;
                }
                else if (strcmp(arg, "platform") == 0) {
                    if (argc < 3) {
                        std::cerr << "Missing argument option for --"
                                  << argv[0] << "\n";
                        usage(prgname);
                        return 1;
                    }
                    worker.d_platform[argv[1]] = argv[2];
                    argv += 2;
                    argc -= 2;
                }
                else if (strcmp(arg, "verbose") == 0) {
                    worker.d_verbose = true;
                }
                else {
                    std::cerr << "Invalid option " << argv[0] << "\n";
                    usage(prgname);
                    return 1;
                }
            }
        }
        else if (worker.d_botID.empty()) {
            worker.d_botID = arg;
        }
        else {
            std::cerr << "Unexpected argument " << arg << "\n";
            usage(prgname);
            return 1;
        }
        argv++;
        argc--;
    }

    if (worker.d_botID.empty()) {
        worker.d_botID = generateDefaultBotID();
    }

    if (!worker.d_botsServer.d_url) {
        std::cerr << "Bots server URL is missing\n";
        usage(prgname);
        return 1;
    }

    if (!worker.d_casServer.d_url) {
        std::cerr << "CAS server URL is missing\n";
        usage(prgname);
        return 1;
    }

    if (worker.d_maxConcurrentJobs > worker.d_stopAfterJobs &&
        worker.d_stopAfterJobs > 0) {
        std::cerr << "WARNING: Max concurrent jobs ("
                  << worker.d_maxConcurrentJobs
                  << ") > number of jobs to stop after ("
                  << worker.d_stopAfterJobs << "). Capping concurrent jobs to "
                  << worker.d_stopAfterJobs << ".";
        worker.d_maxConcurrentJobs = worker.d_stopAfterJobs;
    }

    worker.runWorker();
    return 0;
}
