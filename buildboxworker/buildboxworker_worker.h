/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_WORKER
#define INCLUDED_BUILDBOXWORKER_WORKER

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_temporaryfile.h>

#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>
#include <condition_variable>
#include <google/devtools/remoteworkers/v1test2/bots.pb.h>
#include <map>
#include <mutex>
#include <set>
#include <string>
#include <vector>

namespace buildboxworker {

using namespace buildboxcommon;

namespace proto {
using namespace google::devtools::remoteworkers::v1test2;
using namespace build::bazel::remote::execution::v2;
} // namespace proto

class Worker {
  public:
    /**
     * Connect to the Bots and CAS servers and run jobs until
     * `d_stopAfterJobs` reaches 0.
     */
    void runWorker();

    buildboxcommon::ConnectionOptions d_botsServer;
    buildboxcommon::ConnectionOptions d_casServer;

    const char *d_instance = "";
    std::string d_botID;
    std::map<std::string, std::string> d_platform;
    std::vector<std::string> d_extraRunArgs;

    int d_maxConcurrentJobs = 1;

    // The worker will stop running after executing this many jobs. If this
    // is negative (the default), it will never stop.
    int d_stopAfterJobs = -1;

    const char *d_buildboxRun = "buildbox-run";

    bool d_verbose = false;

  private:
    /**
     * Execute the action stored in the given temporary file.
     */
    proto::ActionResult executeAction(TemporaryFile &actionFile);

    void workerThread(const std::string leaseId);

    proto::BotSession d_session;
    std::mutex d_sessionMutex;

    // Notified when a worker thread finishes a job.
    std::condition_variable d_sessionCondition;

    // Keep track of Lease IDs that have been assigned to a worker
    // Lease IDs are added when the while-loop spawns worker threads and
    // are removed when by the worker threads when they're done.
    std::set<std::string> d_activeJobs;

    // Keep track of Lease IDs that have been accepted by the worker, but
    // whose acceptance has not yet been acknowledged by the server (so we
    // haven't actually started work on them yet).
    std::set<std::string> d_jobsPendingAck;
};

} // namespace buildboxworker
#endif
