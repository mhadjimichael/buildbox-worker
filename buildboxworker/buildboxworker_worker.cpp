/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_worker.h>

#include <buildboxcommon_client.h>

#include <cstring>
#include <exception>
#include <fcntl.h>
#include <google/devtools/remoteworkers/v1test2/bots.grpc.pb.h>
#include <google/devtools/remoteworkers/v1test2/worker.grpc.pb.h>
#include <google/devtools/remoteworkers/v1test2/worker.pb.h>
#include <google/rpc/code.pb.h>
#include <grpcpp/channel.h>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <system_error>
#include <thread>
#include <unistd.h>

namespace buildboxworker {

#define WORKER_LOG_VERBOSE(x)                                                 \
    if (this->d_verbose) {                                                    \
        std::cerr << x << std::endl;                                          \
    }

namespace {
/**
 * Start a subprocess with the given command and return its PID.
 */
static pid_t runWorkerSubprocess(const std::vector<std::string> &command)
{
    const int argc = command.size();
    const char *argv[argc + 1];
    for (int i = 0; i < argc; ++i) {
        argv[i] = command[i].c_str();
    }
    argv[argc] = nullptr;

    const pid_t pid = fork();
    if (pid == -1) {
        throw std::system_error(errno, std::system_category());
    }
    else if (pid == 0) {
        // (runs only in the child)
        const int devNullFD = open("/dev/null", O_RDWR);
        if (devNullFD == -1) {
            perror("/dev/null unavailable");
            _Exit(1);
        }
        // Prevent worker processes from reading buildbox-worker's stdin
        dup2(devNullFD, STDIN_FILENO);
        // Prevent build job output from being written to buildbox-worker's log
        dup2(devNullFD, STDOUT_FILENO);
        dup2(devNullFD, STDERR_FILENO);
        close(devNullFD);
        execvp(argv[0], const_cast<char *const *>(argv));
        perror(nullptr);
        _Exit(1);
    }

    return pid;
}
} // namespace

proto::ActionResult Worker::executeAction(TemporaryFile &actionFile)
{
    actionFile.close();
    TemporaryFile actionResultFile;
    actionResultFile.close();
    std::vector<std::string> command = {this->d_buildboxRun};

    command.insert(command.end(), this->d_extraRunArgs.begin(),
                   this->d_extraRunArgs.end());
    this->d_casServer.putArgs(&command);

    command.push_back(std::string("--action=") + actionFile.name());
    command.push_back(std::string("--action-result=") +
                      actionResultFile.name());

    const auto pid = runWorkerSubprocess(command);
    if (waitpid(pid, nullptr, 0) == -1) {
        throw std::system_error(errno, std::generic_category());
    }

    proto::ActionResult result;
    const int fd = open(actionResultFile.name(), O_RDONLY);
    if (fd == -1) {
        throw std::system_error(errno, std::generic_category());
    }
    result.ParseFromFileDescriptor(fd);
    close(fd);

    WORKER_LOG_VERBOSE("ActionResult " << result.DebugString());

    return result;
}

void Worker::workerThread(const std::string leaseId)
{
    proto::ActionResult result;

    try {
        TemporaryFile actionFile;
        {
            // Get the Action to execute from the session.
            std::lock_guard<std::mutex> lock(this->d_sessionMutex);
            bool found_lease = false;
            for (auto &lease : d_session.leases()) {
                if (lease.id() == leaseId) {
                    if (lease.payload().Is<proto::Action>()) {
                        proto::Action action;
                        lease.payload().UnpackTo(&action);
                        action.SerializeToFileDescriptor(actionFile.fd());
                    }
                    else if (lease.payload().Is<proto::Digest>()) {
                        proto::Digest actionDigest;
                        lease.payload().UnpackTo(&actionDigest);
                        buildboxcommon::Client client;
                        client.init(this->d_casServer);
                        client.download(actionFile.fd(), actionDigest);
                    }
                    else {
                        throw std::runtime_error("Invalid lease payload type");
                    }
                    found_lease = true;
                    break;
                }
            }
            if (!found_lease) {
                return;
            }
        }
        result = executeAction(actionFile);
    }
    catch (const std::exception &e) {
        result.set_exit_code(1);
        WORKER_LOG_VERBOSE("Caught exception in worker: " << e.what());
        result.set_stderr_raw(std::string("Exception in worker: ") + e.what() +
                              std::string("\n"));
    }

    {
        // Store the ActionResult back in the lease.
        //
        // We need to search for the lease again because it could have been
        // moved/cancelled/deleted/completed by someone else while we were
        // executing it.
        std::lock_guard<std::mutex> lock(this->d_sessionMutex);
        for (auto &lease : *this->d_session.mutable_leases()) {
            if (lease.id() == leaseId &&
                lease.state() == proto::LeaseState::ACTIVE) {
                lease.set_state(proto::LeaseState::COMPLETED);
                lease.mutable_result()->PackFrom(result);
            }
        }
        this->d_activeJobs.erase(leaseId);
    }
    this->d_sessionCondition.notify_all();
}

void Worker::runWorker()
{
    WORKER_LOG_VERBOSE("Starting build worker with bot ID " + d_botID + "");

    this->d_session.set_bot_id(this->d_botID);
    this->d_session.set_status(proto::BotStatus::OK);

    auto workerProto = this->d_session.mutable_worker();
    auto device = workerProto->add_devices();
    device->set_handle(this->d_botID);

    for (const auto &platformPair : this->d_platform) {
        // TODO Differentiate worker properties and device properties?
        auto workerProperty = workerProto->add_properties();
        workerProperty->set_key(platformPair.first);
        workerProperty->set_key(platformPair.second);

        auto deviceProperty = device->add_properties();
        deviceProperty->set_key(platformPair.first);
        deviceProperty->set_key(platformPair.second);
    }

    auto stub = proto::Bots::NewStub(this->d_botsServer.createChannel());

    {
        // Send the initial request to create the bot session.
        grpc::ClientContext context;
        proto::CreateBotSessionRequest createRequest;
        WORKER_LOG_VERBOSE("Setting parent");
        createRequest.set_parent(this->d_instance);
        *createRequest.mutable_bot_session() = this->d_session;
        WORKER_LOG_VERBOSE("Setting session");
        const auto status = stub->CreateBotSession(&context, createRequest,
                                                   &(this->d_session));
        if (!status.ok()) {
            throw std::runtime_error(
                "Failed to create bot session: gRPC error " +
                to_string(status.error_code()) + ": " +
                status.error_message());
        }
    }

    WORKER_LOG_VERBOSE("Bot Session created. Now waiting for jobs.");

    bool skipPollDelay = true;

    while (this->d_stopAfterJobs != 0 || this->d_activeJobs.size() > 0 ||
           this->d_jobsPendingAck.size() > 0) {
        std::unique_lock<std::mutex> lock(this->d_sessionMutex);
        if (skipPollDelay) {
            skipPollDelay = false;
        }
        else {
            // Wait for a build job to finish or 250 ms (whichever comes first)
            this->d_sessionCondition.wait_for(lock,
                                              std::chrono::milliseconds(250));
        }
        for (auto &lease : *this->d_session.mutable_leases()) {
            if (lease.state() == proto::LeaseState::PENDING) {
                WORKER_LOG_VERBOSE("Got lease: " << lease.DebugString());
                if (lease.payload().Is<proto::Action>() ||
                    lease.payload().Is<proto::Digest>()) {
                    if (this->d_activeJobs.size() +
                                this->d_jobsPendingAck.size() <
                            this->d_maxConcurrentJobs &&
                        this->d_stopAfterJobs != 0) {
                        // Accept the lease, but wait for the server's ack
                        // before actually starting work on it.
                        lease.set_state(proto::LeaseState::ACTIVE);
                        skipPollDelay = true;
                        this->d_jobsPendingAck.insert(lease.id());

                        if (this->d_stopAfterJobs > 0) {
                            this->d_stopAfterJobs--;
                        }
                    }
                }
                else {
                    lease.set_state(proto::LeaseState::COMPLETED);
                    auto status = lease.mutable_status();
                    status->set_message("Invalid lease");
                    status->set_code(google::rpc::Code::INVALID_ARGUMENT);
                }
            }
            else if (lease.state() == proto::LeaseState::ACTIVE) {
                this->d_jobsPendingAck.erase(lease.id());
                if (this->d_activeJobs.count(lease.id()) == 0) {
                    const std::string leaseId = lease.id();
                    auto thread = std::thread(
                        [this, leaseId] { this->workerThread(leaseId); });
                    thread.detach();
                    // (the thread will remove its job from activeJobs when
                    // it's done)
                    this->d_activeJobs.insert(lease.id());
                }
            }
            else if (lease.state() == proto::LeaseState::CANCELLED) {
                if (this->d_jobsPendingAck.count(lease.id()) != 0) {
                    // We accepted the job, but the server decided that we
                    // shouldn't run it after all.
                    this->d_jobsPendingAck.erase(lease.id());
                    if (this->d_stopAfterJobs >= 0) {
                        this->d_stopAfterJobs++;
                    }
                }
            }
        }
        {
            // Update the bot session (send our state to the server, then
            // replace our state with the server's response)
            grpc::ClientContext context;
            proto::UpdateBotSessionRequest updateRequest;
            updateRequest.set_name(d_session.name());
            *updateRequest.mutable_bot_session() = this->d_session;
            const auto status = stub->UpdateBotSession(&context, updateRequest,
                                                       &(this->d_session));
            if (!status.ok()) {
                throw std::runtime_error(
                    "Failed to update bot status: gRPC error " +
                    to_string(status.error_code()) + ": " +
                    status.error_message());
            }
        }
    }
}
} // namespace buildboxworker
