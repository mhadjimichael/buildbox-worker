FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest

RUN apt-get update

COPY . /buildbox-worker

ENV BUILDBOX_COMMON_SOURCE_ROOT=/buildbox-common
ENV CMAKE_OPTS="-DBUILD_TESTING=OFF"

RUN cd /buildbox-worker && mkdir -p build && cd build && \
    cmake ${CMAKE_OPTS} .. && make

ENV PATH "/buildbox-worker/build:$PATH"
